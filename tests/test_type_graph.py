import pytest

import pokemon
from pokemon.type_graph import get_type_table, filler, unfold_table, plot_chart

class TestTypeGraphParser():
    #def test_get_type_table(self):
    #    table = get_type_table()

    #def test_filler(self):
    #    pass
    #    table = get_type_table()
    #    filler(chart, attack_type, deflist, value):

    #def test_unfold_table(self):
    #    table = get_type_table()
    #    chart = unfold_table(table)

    def test_plot_chart(self):
        table = get_type_table()
        chart = unfold_table(table)
        fig = plot_chart(chart)
