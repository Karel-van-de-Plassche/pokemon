import requests
from html.parser import HTMLParser
from ast import literal_eval
from collections import UserDict

import pandas as pd
import numpy as np
from IPython import embed
import logging
import matplotlib
import matplotlib as mpl
import matplotlib.pyplot as plt

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

class TypeGraphParser(HTMLParser):
    table_found = None
    table_title = ""
    header = []
    header_ready = False
    rows = []

    def handle_starttag(self, tag: str, attrs: list):
        # Attr 2-tuple
        #tag=div, attrs=[('class', 'c-interactive-table'), ('data-cid', 'apps/interactive_table-1669221247_2343_114649'), ('data-cdata', '{"paging":false,"searchable":false,"sort_column":0,"sort_descending":false,"sortable":false}')]
        def is_table(attrs):
            for attr in attrs:
                if attr[0] == "class" and attr[1] == "c-interactive-table":
                    return True
            return False

        if tag == "div" and is_table(attrs):
            #"class" in attrs:
            logger.debug(f"tag={tag}, attrs={attrs}")
            self.table_found = True
            pos: tuple = self.getpos()

    def handle_endtag(self, tag: str):
        if tag == "div" and self.table_found is True:
            self.table_found = False

    def handle_data(self, data: str):
        rdata = data.strip()
        if self.table_found is True and rdata != "":
            if self.table_title == "":
                print(f"Found a title! id(self)={id(self)}")
                self.table_title = rdata
            elif len(self.header) < 6 and not self.header_ready:
                # The table has 3 columns
                self.header.append(rdata)
            else:
                if len(self.header) == 6:
                    assert self.header[:3] == self.header[3:], self.header
                    self.header = self.header[:3]
                    self.header_ready = True
                if len(self.rows) == 0:
                    self.rows.append([rdata])
                elif len(self.rows[-1]) < 3:
                    self.rows[-1].append(rdata)
                elif len(self.rows[-1]) == 3:
                    self.rows.append([rdata])
                else:
                    raise Exception

TYPE_GRAPH_URL="https://www.polygon.com/pokemon-scarlet-violet-guide/23451361/type-chart-strengths-weaknesses-super-effective"
IMMUNITIES = [
    ("Normal", "Ghost"),
    ("Ground", "Electric"),
    ("Flying", "Ground"),
    ("Ghost", "Normal"),
    ("Ghost", "Fighting"),
    ("Dark", "Psychic"),
    ("Steel", "Poison"),
]

def get_type_table():
    get_url = f"{TYPE_GRAPH_URL}"
    rr = requests.get(get_url)
    logger.info(f"Getting %s", get_url)
    if rr.status_code == 200:
        parser = TypeGraphParser()
        parser.feed(rr.text)
    # This is the attack chart
    table = pd.DataFrame(data=parser.rows, columns=parser.header)
    table.name = parser.table_title
    return table

def filler(chart, attack_type, deflist, value):
    for type_ in deflist:
        if type_ != "--":
            pre_val = chart.loc[attack_type, type_]
            if np.isnan(pre_val):
                chart.loc[attack_type, type_] = value

def unfold_table(table: pd.DataFrame):
    # We are not using the index right now
    def has_immunity(type_):
        for el in IMMUNITIES:
            attack, defense = el
            if attack == type_:
                return True
        return False

    types = table["Type"]
    chart = pd.DataFrame(index=types, columns=types)
    chart.name = table.name
    for ind, row in table.iterrows():
        names: pd.Index = row.index
        attack_type = row["Type"]
        strong_vs = row["Strong Against"]
        strong_vs = [el.strip() for el in strong_vs.split(",")]
        weak_vs = row["Weakness"]
        weak_vs = [el.strip() for el in weak_vs.split(",")]
        chart.index.name = "Attacking move"
        chart.columns.name = "Defending type"

        # Fill in weak + strong
        filler(chart, attack_type, strong_vs, 2)
        filler(chart, attack_type, weak_vs, 0.5)

        # Fill in immumities
        if has_immunity(attack_type):
            for el in IMMUNITIES:
                attack, defense = el
                if attack == attack_type:
                    immunities = [el for el in IMMUNITIES if el[0] == attack_type]
        else:
            immunities = []

        for attack, defense in immunities:
            chart.loc[attack, defense] = 0

    # Fill in unfilled
    chart[chart.isnull()] = 1

    return chart

def plot_chart(chart):
    fig, ax = plt.subplots()
    chart[chart == 1] = np.nan
    im = ax.imshow(np.float64(chart))

    # Show all ticks and label them with the respective list entries
    ax.set_xticks(np.arange(len(chart.index)), labels=chart.index)
    ax.set_yticks(np.arange(len(chart.columns)), labels=chart.columns)

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
            rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    # Top left is (0, 0) for us
    for ii in range(len(chart.columns)):
        for jj in range(len(chart.index)):
            text = ax.text(jj, ii,
                           #f"{ii}, {jj}",
                           f"{chart.iloc[ii, jj]}",
                        ha="center", va="center", color="w")

    ax.tick_params(top=True, labeltop=True, bottom=False, labelbottom=False)
    ax.set_ylabel("Attack type")
    ax.set_xlabel("Defender type")

    ax.set_title(chart.name)
    fig.tight_layout()
    return fig

if __name__ == "__main__":
    table = get_type_table()
    chart = unfold_table(table)
    fig = plot_chart(chart)
    plt.show()
